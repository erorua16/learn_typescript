"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dotenv_1 = __importDefault(require("dotenv"));
//////////////////////////////////////////////////////////////DOT ENV CONFIG////////////////////////////////////////////////////////////
//initialize configuration for prod/dev
dotenv_1.default.config();
//import port from dotenv
const port = process.env.SERVER_PORT;
////////////////////////////////////////////////////////////////EXPRESS///////////////////////////////////////////////////////////////
const app = (0, express_1.default)();
app.get("/", (req, res) => {
    res.send("hello world!");
});
app.listen(port, () => {
    //tslint:disable-next-line:no-console
    console.log(`server started at http://localhost:${port}`);
});
//# sourceMappingURL=index.js.map