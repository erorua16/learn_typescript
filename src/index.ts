import express from "express";
import dotenv from "dotenv";
import path from "path";



//////////////////////////////////////////////////////////////DOT ENV CONFIG////////////////////////////////////////////////////////////
//initialize configuration for prod/dev
dotenv.config();
//import port from dotenv
const port = process.env.SERVER_PORT || 9000;


////////////////////////////////////////////////////////////////EXPRESS///////////////////////////////////////////////////////////////
const app = express();



app.get("/", (req, res)=> { 
    res.send("hello world!");
});

app.listen( port, ()=> {
    //tslint:disable-next-line:no-console
    console.log(`server started at http://localhost:${port}`);
});